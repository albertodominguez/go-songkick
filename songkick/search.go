package songkick

import (
	"errors"
	"fmt"
	"net/http"
	"time"
)

const (
	EventTypeConcert  = "Concert"
	EventTypeFestival = "Festival"

	SearchTypeEvent    = "event"
	SearchTypeArtist   = "artist"
	SearchTypeVenue    = "venue"
	SearchTypeLocation = "location"
)

// Options Represents possible options for a search query
type Options struct {
	endpoint   string
	searchType string
	params     map[string]string

	Page  int64
	Limit int64
}

// GetOptions Returns a base Options object that includes specific API endpoint
func (c *Client) GetOptions(t string) (opt *Options, err error) {
	opt = &Options{
		params: make(map[string]string),
		Page:   1,
		Limit:  50,
	}

	switch t {
	case SearchTypeEvent:
		opt.endpoint = "/events.json"
	case SearchTypeArtist:
		opt.endpoint = "/search/artists.json"
	case SearchTypeVenue:
		opt.endpoint = "/search/venues.json"
	case SearchTypeLocation:
		opt.endpoint = "/search/locations.json"
	default:
		return nil, errors.New("invalid search type")
	}

	opt.searchType = t
	return
}

// Query Adds 'query' parameter to search options
func (o *Options) Query(q string) *Options {
	if o.searchType != SearchTypeEvent {
		o.params["query"] = q
	}

	return o
}

// Artists Adds 'artist_id' parameter to search options
func (o *Options) Artist(a string) *Options {
	if o.searchType == SearchTypeEvent {
		o.params["artist_name"] = a
	} else {
		o.params["query"] = a
	}

	return o
}

// Before Adds 'end_date' parameter to search options
func (o *Options) Before(d time.Time) *Options {
	if o.searchType == SearchTypeEvent {
		o.params["max_date"] = d.Format("2006-01-02")
	}

	return o
}

// After Adds 'start_date' parameter to search options
func (o *Options) After(d time.Time) *Options {
	if o.searchType == SearchTypeEvent {
		o.params["min_date"] = d.Format("2006-01-02")
	}

	return o
}

// Between Adds 'start_date' and 'end_date' parameters to search options
func (o *Options) Between(from time.Time, to time.Time) *Options {
	if o.searchType == SearchTypeEvent {
		o.params["min_date"] = from.Format("2006-01-02")
		o.params["max_date"] = to.Format("2006-01-02")
	}

	return o
}

// EventType Adds 'type' parameter to search options
func (o *Options) EventType(t string) *Options {
	if o.searchType == SearchTypeEvent && (t == EventTypeConcert || t == EventTypeFestival) {
		o.params["type"] = t
	}

	return o
}

// LocationID Adds songkick ID or 'location=sk:{ID}' parameter to search options
func (o *Options) LocationID(ID int) *Options {
	if o.searchType == SearchTypeEvent {
		o.params["location"] = fmt.Sprintf("sk:%d", ID)
	}

	return o
}

// Geolocation Adds geolocation coordinate or 'location=geo:{LAT},{LNG}' parameter to search options
func (o *Options) Geolocation(latitude, longitude string) *Options {
	if o.searchType == SearchTypeEvent || o.searchType == SearchTypeLocation {
		o.params["location"] = fmt.Sprintf("geo:%s,%s", latitude, longitude)
	}

	return o
}

// IP Adds IP as location or 'location=ip:{IP}' filter to search options
func (o *Options) IP(IP string) *Options {
	if o.searchType == SearchTypeEvent || o.searchType == SearchTypeLocation {
		o.params["location"] = fmt.Sprintf("ip:%s", IP)
	}

	return o
}

// IP Adds Client IP as location or 'location=clientip:{IP}' filter to search options
func (o *Options) ClientIP(IP string) *Options {
	if o.searchType == SearchTypeEvent || o.searchType == SearchTypeLocation {
		o.params["location"] = fmt.Sprintf("clientip:%s", IP)
	}

	return o
}

// Serach Performs an API call to specific search endpoint
func (c *Client) Search(opts *Options) (r *Response, err error) {
	r = &Response{}
	err = c.call(r, http.MethodGet, opts)
	if err != nil {
		return
	}

	err = r.IsError()
	return
}
