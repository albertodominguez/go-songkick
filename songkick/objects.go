package songkick

import "time"

type Event struct {
	ID   int64  `json:"id"`
	Type string `json:"type"`
	URI  string `json:"uri"`
	Name string `json:"displayName"`

	Start *Datetime `json:"start"`
	End   *Datetime `json:"end,omitempty"`

	Performance []Performance `json:"performance"`
	Location    Location      `json:"location"`
	Venue       Venue         `json:"venue"`

	Popularity float64 `json:"popularity"`

	Status         string `json:"status"`
	AgeRestriction string `json:"ageRestriction,omitempty"`
}

type Performance struct {
	ID int64 `json:"id"`

	Artist Artist `json:"artist"`
	Name   string `json:"displayName"`

	BillingIndex int64  `json:"billingIndex"`
	Billing      string `json:"billing"`
}

type Artist struct {
	ID          int64           `json:"id"`
	Name        string          `json:"displayName"`
	URI         string          `json:"uri"`
	MusicBrainz []MusicBrainzID `json:"identifier,omitempty"`
	OnTourUntil *time.Time      `json:"onTourUntil,omitempty"`
}

type Location struct {
	City      string   `json:"city"`
	Latitude  *float64 `json:"lat,omitempty"`
	Longitude *float64 `json:"lng,omitempty"`
}

type Venue struct {
	ID          int64  `json:"id"`
	Name        string `json:"displayName"`
	Description string `json:"description,omitempty"`
	URI         string `json:"uri"`

	City    City   `json:"city"`
	Street  string `json:"street,omitempty"`
	ZIPCode string `json:"zip,omitempty"`
	Phone   string `json:"phone,omitempty"`
	Website string `json:"website,omitempty"`

	Capacity *int64 `json:"capacity,omitempty"`

	Latitude  *float64 `json:"lat,omitempty"`
	Longitude *float64 `json:"lng,omitempty"`

	// MetroArea was not included based on response object documentation at
	// https://www.songkick.com/developer/response-objects
	// "... we recommend not using when integrating into a feature..."

	// MetroArea City   `json:"metroArea"`
}

type Datetime struct {
	Time     string `json:"time"`
	Date     string `json:"date"`
	Datetime string `json:"datetime"`
}

type MusicBrainzID struct {
	ID  string `json:"mbid"`
	URL string `json:"href"`
}

type City struct {
	ID      int64   `json:"id"`
	URI     string  `json:"uri"`
	Name    string  `json:"displayName"`
	Country Country `json:"country"`
}

type Country struct {
	Name string `json:"displayName"`
}
