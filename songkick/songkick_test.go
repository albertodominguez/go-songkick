package songkick

import (
	"errors"
	"os"
	"testing"
	"time"
)

func TestClient_SearchArtist(t *testing.T) {
	key := os.Getenv("SONGKICK_API_KEY")
	c := New(key, true)
	if c == nil {
		t.Fatal(errors.New("unable to get a valid API client"))
	}

	opts, err := c.GetOptions(SearchTypeArtist)
	if err != nil {
		t.Fatal(err)
		return
	}

	r, err := c.Search(opts.Artist("Alberto"))
	if err != nil {
		t.Fatal(err)
		return
	}

	if r.Pages() > 0 {
		t.Log(r.Page.Data)
	}
}

func TestClient_SearchEvent(t *testing.T) {
	key := os.Getenv("SONGKICK_API_KEY")
	c := New(key, true)
	if c == nil {
		t.Fatal(errors.New("unable to get a valid API client"))
	}

	opts, err := c.GetOptions(SearchTypeEvent)
	if err != nil {
		t.Fatal(err)
		return
	}

	// Look for Miami, FL events
	r, err := c.Search(opts.LocationID(9776).Between(time.Now(), time.Now().AddDate(0, 0, 7)).EventType(EventTypeConcert))
	if err != nil {
		t.Fatal(err)
		return
	}

	if r.Pages() > 0 {
		t.Log(r.Page.Data)
	}
}
