package songkick

import "errors"

// Response Stores API response object
type Response struct {
	Page Page `json:"resultsPage"`
}

// Page Stores API response pagination
type Page struct {
	Status string `json:"status"`

	Limit      int64 `json:"perPage"`
	Page       int64 `json:"page"`
	TotalItems int64 `json:"totalEntries"`

	Data  ResponseData   `json:"results"`
	Error *ResponseError `json:"error,omitempty"`
}

// Response Stores API response data
type ResponseData struct {
	Events    []Event    `json:"event,omitempty"`
	Artists   []Artist   `json:"artist,omitempty"`
	Venues    []Venue    `json:"venue,omitempty"`
	Locations []Location `json:"location,omitempty"`
}

type ResponseError struct {
	Message string `json:"message,omitempty"`
}

// IsError Return API error call or nil if no error ocurred.
func (r *Response) IsError() error {
	if r.Page.Status == "error" {
		if r.Page.Error != nil {
			return errors.New(r.Page.Error.Message)
		}

		return errors.New("unknown error")
	}

	return nil
}

// LastPage Returns if current response holds last set of data
func (r *Response) LastPage() bool {
	return r.Page.Page == r.Pages()
}

// Empty Validates if response data is empty
func (r *Response) Empty() bool {
	return r.Page.TotalItems == 0
}

// Returns the total number of pages
func (r *Response) Pages() int64 {
	if r.Page.Limit > 0 {
		return (r.Page.TotalItems / r.Page.Limit) + 1
	}

	return -1
}
