package songkick

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

const baseURL = "https://api.songkick.com/api/3.0"

// Client Represents Songkick API client
type Client struct {
	verbose    bool
	apiKey     string
	httpClient *http.Client
}

// New Returns a new API client using provided API Key (k parameter)
func New(k string, verbose ...bool) *Client {
	v := (len(verbose) > 0 && verbose[0])
	return &Client{apiKey: k, httpClient: &http.Client{Timeout: time.Minute}, verbose: v}
}

func (c *Client) call(e interface{}, method string, opts *Options) (err error) {
	query := new(bytes.Buffer)

	if opts != nil {
		for key, value := range opts.params {
			fmt.Fprintf(query, "&%s=%s", key, value)
		}

		if opts.Page > 1 {
			fmt.Fprintf(query, "&page=%d", opts.Page)
		}

		if opts.Limit > 0 && opts.Limit < 50 {
			fmt.Fprintf(query, "&per_page=%d", opts.Limit)
		}
	}

	var URL = fmt.Sprintf("%s%s?apikey=%s%s", baseURL, opts.endpoint, c.apiKey, query.String())
	if c.verbose {
		log.Printf("[Songkick] DEBUG - Calling %s : %s", method, URL)
	}

	req, err := http.NewRequest(method, URL, nil)
	if err != nil {
		log.Printf("unable to create the HTTP request: %v", err)
		return
	}
	req.Header.Add("Accept", "application/json")

	res, err := c.httpClient.Do(req)
	if err != nil {
		log.Printf("unable to send the HTTP request: %v", err)
		return
	}

	defer res.Body.Close()
	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Printf("unable to load the API response: %v", err)
	}

	if c.verbose {
		log.Printf("[Songkick] DEBUG - Receiving: %s", b)
	}

	json.Unmarshal(b, e)
	return
}
