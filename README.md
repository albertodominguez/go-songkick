# go-songkick

Go Songkick is a go API client for interacting with some of the
[Songkick API](https://www.songkick.com/developer/getting-started)

## Install

This is a small client library for Songkick search API. Works fine with Go
Modules.

## Usage

```go
import "gitlab.com/albertodominguez/go-songkick/songkick"
```

### 1. Get an API client using you API Key

```go
clt = songkick.New(YOUR_API_KEY)
```

 ### 2. Get a search options object. This struct holds information about
your query parameters to perform your search request

```go
opts, err = clt.GetOptions(SEARCH_TYPE)
```

`SEARCH_TYPE` is one of the following. You can use the constants defined

```go
const (
	SearchTypeEvent    = "event"
	SearchTypeArtist   = "artist"
	SearchTypeVenue    = "venue"
	SearchTypeLocation = "location"
)
```

For example:

```go
opts, err = clt.GetOptions(songkick.SearchTypeArtist)
```

### 3. Set you filters and options

```go
opts.Artist("ARTIST NAME")
opts.Page = 3 // Pages start on 1 (not 0)
opts.Limit = 10 // API max limit is 50
```

### 4. Make your API call

```go
response, err = clt.Search(opts)
```
### Tip: You can combine filters on your search

```go
clt = songkick.New(YOUR_API_KEY)
opts, err = clt.GetOptions(songkick.SearchTypeEvent)
if err != {
    // Handle error here
}

// This will search for Concerts within the next month for `Artist`
response, err = clt.Search(opts.Artist("Artist").Between(time.Now(), time.Now().AddDate(0, 1, 0)).EventType(songkick.EventTypeConcert))
```

