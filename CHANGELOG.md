# Changelog

## [v0.1.3] (2021-05-02)

**Enhancements:**

- Adds Response.LastPage() to validate if current data set is the last
- Adds Response documentation

**Bug fixes:**

- Fixes README.md errors
- Fix Response.Empty() validation

## [v0.1.2] (2021-05-02)

**Enhancements:**

- Handles Page (page) and Limit (per_page) parameter values
- Including README.md

**Bug fixes:**

- Pagination was not included during API calls
- Adds support to different response "results" objects (events, artists, venues and locations)
- Better code distribution through different files
## [v0.1.1] (2021-05-02)

**Enhancements:**

- Adds Response struct for better API call response management
- Adds support to different response "results" objects (events, artists, venues and locations)
- Better code distribution through different files
## [v0.1.0] (2021-05-01)

- Initial tagged release